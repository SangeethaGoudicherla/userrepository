package com.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.feignClient.OrderFeignClient;
import com.user.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	OrderFeignClient orderFeignClient;
	
	@Override
	public String getPort() {
		// TODO Auto-generated method stub
		System.out.println("In port-----------------------------");
		return orderFeignClient.getPort();
	}

}
