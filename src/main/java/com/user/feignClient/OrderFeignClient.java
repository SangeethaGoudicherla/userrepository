package com.user.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name="http://ORDER-SERVICE")		
public interface OrderFeignClient {
	@GetMapping("/api/order/port")
	public String getPort();
}
